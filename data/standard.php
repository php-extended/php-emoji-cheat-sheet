<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-emoji-cheat-sheet library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

return [
	':)' => 'simple_smile',
	':-)' => 'simple_smile',
	':))' => 'smile',
	':-))' => 'smile',
	':D' => 'smile',
	':-D' => 'smile',
	';)' => 'wink',
	';-)' => 'wink',
	':p' => 'stuck_out_tongue',
	':-p' => 'stuck_out_tongue',
	':(' => 'confused',
	':-(' => 'confused',
	':((' => 'confounded',
	':-((' => 'confounded',
	'8)' => 'sunglasses',
	'8-)' => 'sunglasses',
	'<3' => 'heart',
	':o' => 'open_mouth',
	':-o' => 'open_mouth',
	':3' => 'smiley_cat',
	':-3' => 'smiley_cat',
	':|' => 'expressionless',
	':-|' => 'expressionless',
	'o:)' => 'innocent',
	'o:-)' => 'innocent',
	'0:)' => 'innocent',
	'0:-)' => 'innocent',
	'O:)' => 'innocent',
	'O:-)' => 'innocent',
	':\'(' => 'cry',
	':\'-(' => 'cry',
	':?' => 'question',
];
