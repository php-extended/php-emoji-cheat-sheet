<?php declare(strict_types=1);

$fullhtml = file_get_contents('https://raw.githubusercontent.com/WebpageFX/emoji-cheat-sheet.com/master/public/index.html');
if($fullhtml === false)
	throw new Exception('Impossible to fetch raw contents on github.');

$extended_file_path = __DIR__.'/src/extended.php';
$extended = require $extended_file_path;

echo "Processing source list\n";

$matches = array();
// i know, it's not recommanded to parse html with regexes
preg_match_all('#<div><img src="graphics/emojis/(.*?)\.png">:<span class="name" data-alternative-name="(.*?)">(.*?)</span>:</div>#ui', $fullhtml, $matches, PREG_SET_ORDER);

foreach($matches as $matchset)
{
	$filename = trim(str_replace('"', '', html_entity_decode($matchset[1])));
	$default = trim(str_replace('"', '', html_entity_decode($matchset[3])));
	// force set for exact smilies
	$extended[$default] = $filename;
	
	foreach(array_map('trim', explode(',', $matchset[2])) as $matchname)
	{
		// avoid override for others
		$matchfname = str_replace('"', '', html_entity_decode($matchname));
		if(!isset($extended[$matchfname]))
		{
			$extended[$matchfname] = $filename;
		}
	}
}

ksort($extended);

file_put_contents($extended_file_path, '<?php return '.var_export($extended, true).';'."\n");

foreach(array_unique(array_values($extended)) as $emojiname)
{
	echo "Processing emoji ".$emojiname."\n";
	$targetfile = __DIR__.'/img/'.$emojiname.'.png';
	if(!is_file($targetfile))
	{
		$contents = file_get_contents('https://raw.githubusercontent.com/WebpageFX/emoji-cheat-sheet.com/master/public/graphics/emojis/'.urlencode($emojiname).'.png');
		if($contents === false)
		{
			echo "Impossible to get file for emoji ".$emojiname."\n";
			continue;
		}
		file_put_contents($targetfile, $contents);
	}
}

echo "Processing complete.\n";
