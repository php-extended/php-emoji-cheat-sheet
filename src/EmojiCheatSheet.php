<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-emoji-cheat-sheet library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\EmojiCheatSheet;

use Stringable;

/**
 * Emoji class file.
 *
 * This class is a wrapper to retrieve the image names from the given emoji
 * strings.
 *
 * @author Anastaszor
 */
class EmojiCheatSheet implements Stringable
{
	
	/**
	 * The standard emoji name to file name map.
	 *
	 * @var array<string, string>
	 */
	protected array $_standard = [];
	
	/**
	 * The extended emoji name to file name map.
	 *
	 * @var array<string, string>
	 */
	protected array $_extended = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the path of the directory where all the images are stored.
	 *
	 * @return string
	 */
	public function getImagesDirectoryPath() : string
	{
		return \dirname(__DIR__).\DIRECTORY_SEPARATOR.'img';
	}
	
	/**
	 * Gets the emoji file name from the given supported emoji.
	 *
	 * @param string $emoji
	 * @return ?string null if not found
	 */
	public function getImageName(string $emoji) : ?string
	{
		$standard = self::getStandardElements();
		if(isset($standard[$emoji]))
		{
			return $standard[$emoji];
		}
		
		$extended = self::getExtendedElements();
		if(isset($extended[$emoji]))
		{
			return $extended[$emoji];
		}
		
		return null;
	}
	
	/**
	 * Gets all the emoji names that are supported natively, like :-).
	 *
	 * @return array<int, string>
	 */
	public function getSupportedStandardEmoji() : array
	{
		return \array_keys($this->getStandardElements());
	}
	
	/**
	 * Gets all the emoji names that are supported like :emoji_name:.
	 *
	 * @return array<int, string>
	 */
	public function getSupportedExtendedEmoji() : array
	{
		return \array_keys($this->getExtendedElements());
	}
	
	/**
	 * Gets the file name map for all supported emoji with native syntax.
	 *
	 * @return array<string, string>
	 */
	protected function getStandardElements() : array
	{
		if([] === $this->_standard)
		{
			$data = require \dirname(__DIR__).\DIRECTORY_SEPARATOR.'data'.\DIRECTORY_SEPARATOR.'standard.php';
			
			foreach($data as $key => $value)
			{
				$this->_standard[(string) $key] = (string) $value;
			}
		}
		
		return $this->_standard;
	}
	
	/**
	 * Gets the file name map for all supported emoji with extended syntax.
	 *
	 * @return array<string, string>
	 */
	protected function getExtendedElements() : array
	{
		if([] === $this->_extended)
		{
			$data = require \dirname(__DIR__).\DIRECTORY_SEPARATOR.'data'.\DIRECTORY_SEPARATOR.'extended.php';
			
			foreach($data as $key => $value)
			{
				$this->_extended[(string) $key] = (string) $value;
			}
		}
		
		return $this->_extended;
	}
	
}
