# php-extended/php-emoji-cheat-sheet
A wrapper around the WebpageFX/emoji-cheat-sheet.com emoji repository

![coverage](https://gitlab.com/php-extended/php-emoji-cheat-sheet/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-emoji-cheat-sheet/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-emoji-cheat-sheet ^8`


## Basic Usage

This library gives only one class and the following API:

```php

	use PhpExtended\EmojiCheatSheet\Emoji;

	// method that returns the path on the disk where all the
	// emoji images are located.
	Emoji::getImagesDirectoryPath():string;
	
	// method that returns an array with all the standard
	// emoji (like ":-)") which are to be interpreted as-is
	Emoji::getSupportedStandardEmoji():string[];
	
	// method that returns an array with all the extended
	// emoji (like "happy") which are to be interpreted in
	// a customized wrapper (recommanded : use ":happy:"
	// with a ":<name>:" wrapper
	Emoji::getSupportedExtendedEmoji():string[];
	
	// method that returns the standardized name of the emoji
	// for which we are sure that a file exists
	// returns null if there is no known file for given $emoji
	Emoji::getImageName(string $emoji):string;
	
```

This library may be used the following way:

```php

	use PhpExtended\EmojiCheatSheet\Emoji;
	
	$text = '<put the user text with emoji here :happy:>';
	$text = htmlentities($text);
	
	foreach(Emoji::getSupportedExtendedEmoji() as $emoji)
	{
		$replace = '<img src="/path/to/filedir/'.Emoji::getImageName($emoji).'.png" alt="'.$emoji.'">';
		$text = str_replace(':'.$emoji.':', $replace, $text);
	}
	
	echo $text;
	
```


## License

MIT (See [license file](LICENSE)).


## TODO

- Get a wrapper for unicode chars (full list at http://unicode.org/emoji/charts/full-emoji-list.html)
- Get a translation for non-english languages 
